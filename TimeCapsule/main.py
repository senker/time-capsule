import tkinter
from tkinter.messagebox import askyesno

from tkinter import *
from tkinter import messagebox

base_window = Tk()  # Initializing the Tkinter
frame = Frame(base_window)  # Placing all the elements inside the window in a frame called "frame"
frame.pack()  # Packing the widget inside the parent widget
bottom_frame = Frame(base_window)
bottom_frame.pack(side=BOTTOM)
text_label = Label(frame, text="Insert in the window below the desired text for the Time Capsule",
                   relief=RAISED, height=2)  # Creating a text label with a hardcoded message
text_label.pack(side=TOP)  # Selecting the position of the text_label inside the frame
text_entry = Entry(frame, bd=1, width=56)  # Creating a text entry for the user
text_entry.pack(side=LEFT)
lock_file = Checkbutton(frame, text="Lock the file", variable=1, onvalue=1, offvalue=0, height=3, width=11)
lock_file.pack(side=RIGHT)  # Checkbutton functionality not implemented yet
# Checkbutton purpose is to trigger a calendar where you can pick a date until you want the file to be locked


def time_capsule_text():
    messagebox.askyesno("Save", "Do you wish to save this file?")
    inserted_text = text_entry.get()
    time_capsule_user = open('timecapsule.txt', 'w')
    time_capsule_user.write(inserted_text)


save_Button = tkinter.Button(bottom_frame, text="Save", command=time_capsule_text)  # Initializing the button
# while referencing the time_capsule_text() function through: command=time_capsule_text
save_Button.pack(side=BOTTOM)  # Positioning save button on the bottom of the window
base_window.mainloop()  # Adding everything to a mainloop which will keep the window running

